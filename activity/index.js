// List of the array
console.log("Original Array: ");
const listOfWrestler = [
    "Dwayne Johnson",
    "Steve Austin",
    "Kurt Angle",
    "Dave Bautista",
];
console.log(listOfWrestler);

// Create a function which is able to receive a single argument and add the input at the end of the users array.
function manipulateArray(string) {
    listOfWrestler[4] = string;
    console.log(listOfWrestler);
}
manipulateArray("John Cena");

// Create a function which is able to receive an index number as a single argument return the item accessed by its index.
function displayIndex(indexNum) {
    return listOfWrestler[indexNum];
}
const itemFound = displayIndex(2);
console.log(itemFound);

// Create a function which is able to delete the last item in the array and return the deleted item.
function deleteIndex() {
    const lastIndexOfArray = listOfWrestler[listOfWrestler.length - 1];
    listOfWrestler.length = listOfWrestler.length - 1;
    return lastIndexOfArray;
}
const deleteIndexResult = deleteIndex();
console.log(deleteIndexResult);
console.log(listOfWrestler);

// Create a function which is able to update a specific item in the array by its index.
function updateSpecificIndex(indexNum, update) {
    listOfWrestler[indexNum] = update;
}
updateSpecificIndex(3, "Triple H");
console.log(listOfWrestler);

// Create a function which is able to delete all items in the array.
function deleteAllIndex(arrName) {
    arrName.length = 0;
}
deleteAllIndex(listOfWrestler);
console.log(listOfWrestler);

// Create a function which is able to check if the array is empty.
function isArrayEmpty() {
    if (listOfWrestler.length > 0) {
        return false;
    } else {
        return true;
    }
}
let isUsersEmpty = isArrayEmpty();
console.log(isUsersEmpty);
